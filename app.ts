import express from 'express'
const hbs = require("hbs");
require("dotenv").config();
const app = express();

app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

//importamos el archivos de rutas
const router = require("./routes/public.js");

app.use("/", router);


const puerto = process.env.PORT
app.listen(puerto, () => {
         console.log("El servidor se está ejecutando en http://localhost:" + puerto);
});
