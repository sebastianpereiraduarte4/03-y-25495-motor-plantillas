INSERT INTO integrantes (matricula, nombre, apellido) VALUES
('Y25495', 'Sebastian', 'Pereira'),
('Y19937', 'Juan', 'Aquino'),
('Y25387', 'Junior', 'Cabral'),
('Y19099', 'Elvio', 'Aguero'),
('UG0085', 'Luis', 'Delgado');

INSERT INTO media (idMedia, src, url, matricula,id, alt) VALUES
('1', NULL, 'https://www.youtube.com/embed/FRn6xXXF-7s?si=yVJP3egE0MB4siuE', 'Y25495', '1', 'Trailer de attack of titan'),
('2','/assets/Sebastian-Dibujo.jpg', NULL, 'Y25495', '2', 'Dibujo una fogata'),
('3','/assets/Sebastian-foto.webp', NULL, 'Y25495', '3', 'Dark Souls'),

('4',NULL, 'https://www.youtube.com/embed/_Yhyp-_hX2s', 'Y19937', '1', 'Canción de Eminem'),
('5', '/assets/Juan-Dibujo.jpg', NULL, 'Y19937', '2', 'canasta de basketball'),
('6','/assets/Juan-foto.jpeg', NULL, 'Y19937', '3', 'aro de basketball'),

('7',NULL, 'https://www.youtube.com/embed/Tsnyq-3k7Bg?si=-Bzir8axv4WRU4MS&controls=0', 'Y25387', '1','Video de Quantum fracture'),
('8','/assets/Junior-Dibujo.png', NULL, 'Y25387', '2','Dibujo de jupiter'),
('9','/assets/Junior - Foto.jpg', NULL, 'Y25387', '3', 'Atardecer'),

('10',NULL, 'https://www.youtube.com/embed/NynNdkY2gx0?si=nzQjEF5y6hY6uSOp', 'Y19099', '1', 'Video de jugadas de futbol'),
('11','/assets/Elvio-Dibujo.png', NULL, 'Y19099', '2', 'Dibujo de un partido de futbol'),
('12','/assets/Elvio-Foto.jpg', NULL, 'Y19099', '3', 'El bicho, Cristiano Ronaldo'),

('13',NULL, 'https://www.youtube.com/embed/Hv5ET7azgEk?si=WxryTH3oIMGTTD-Z', 'UG0085', '1', 'Video de la vida secreta de la mente'),
('14','/assets/Luis Delgado-2.png', NULL, 'UG0085', '2', 'Dibujo de una PC'),
('15','/assets/Luis Delgado.jpeg', NULL, 'UG0085', '3','Foto personal en un cerro');


INSERT INTO tipoMedia (id, nombre) VALUES
( '1', 'Youtube'),
('2', 'Dibujo'),
('3', 'Imagen');
