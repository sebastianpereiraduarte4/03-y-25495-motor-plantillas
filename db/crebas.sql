CREATE TABLE IF NOT EXISTS "integrantes" (
	"matricula" TEXT NOT NULL UNIQUE,
	"nombre" TEXT NOT NULL,
	"apellido" TEXT NOT NULL,
	PRIMARY KEY("matricula")
);

CREATE TABLE IF NOT EXISTS "media" (
"idMedia" INTEGER NOT NULL UNIQUE,
"src" TEXT,
"url" TEXT,
"alt" TEXT NOT NULL,
"matricula" TEXT,
"id" TEXT,
PRIMARY KEY("idMedia")
FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
ON UPDATE NO ACTION ON DELETE NO ACTION
FOREIGN KEY ("id") REFERENCES "tipoMedia"("id")
ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS "tipoMedia" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	PRIMARY KEY("id")
);

ALTER TABLE media ADD COLUMN url TEXT;